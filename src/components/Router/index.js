import React from 'react'

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import Menu from '../Menu'
import PrivateRoute from '../PrivateRoute'

import Home from '../../screens/home'
import Login from '../../screens/login'
import NoPage from '../../screens/404'
import Teams from '../../screens/teams'
import Competition from '../../screens/competition'
import Favoris from '../../screens/favoris'
import NoLog from '../../screens/noLog'
import Logout from '../../screens/logout'

const RouterMenu = () => {
  return (
    <Router>
      <Menu></Menu>
      <Switch>
        <Route exact path='/' component={Home}></Route>
        <Route path='/login' component={Login}></Route>
        <Route path='/logout' component={Logout}></Route>
        <Route path='/competition/:id' component={Competition}></Route>
        <Route path='/teams/:id' component={Teams}></Route>
        <Route path='/noLog' component={NoLog}></Route>
        <PrivateRoute path='/favoris' component={Favoris}></PrivateRoute>
        <Route path='*'>
          <NoPage></NoPage>
        </Route>
      </Switch>
    </Router>
  )
}

export default RouterMenu
