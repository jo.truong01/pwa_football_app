import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const PageCreation = (total, setPageCount) => {
  let pageElement = []

  for (let i = 1; i <= total / 10; i++) {
    pageElement.push(
      <LiStyled
        onClick={() => {
          setPageCount(i)
        }}
      >
        {i}
      </LiStyled>
    )
  }
  return pageElement
}

const Pagination = ({ total, setPageCount }) => {
  return <UlStyled>{PageCreation(total, setPageCount)}</UlStyled>
}

const LiStyled = styled.li`
  &:hover {
    color: red;
  }
`

const UlStyled = styled.ul`
  display: block;
  background-color: #dcdde1;
`

Pagination.propTypes = {
  total: PropTypes.number,
  setPageCount: PropTypes.func
}

export default Pagination
