import React, { useState } from 'react'
import Axios from 'axios'

import './loginForm.css'

import styled from 'styled-components'
import { useHistory } from 'react-router-dom'

const LoginForm = () => {
  const [password, setPassword] = useState('')
  const [email, setEmail] = useState('')
  let history = useHistory()

  function handleMdp(event) {
    setPassword(event.target.value)
  }

  function handleEmail(event) {
    setEmail(event.target.value)
  }

  function handleLog() {
    const cred = { username: email, password: password }
    Axios.post('https://easy-login-api.herokuapp.com/users/login', cred)
      .then(resp => {
        localStorage.setItem('Token', resp.headers['x-access-token'])
        history.push('/')
      })
      .catch(error => {
        console.log('ERROR: ', error)
      })
  }

  return (
    <div>
      Login form
      <form>
        <label>Email:</label>
        <InputStyled
          type='email'
          name='username'
          onChange={handleEmail}
          placeholder='Email'
        ></InputStyled>
        <label>Mot de passe:</label>
        <InputStyled
          type='password'
          name='password'
          onChange={handleMdp}
          placeholder='Mot de passe'
        ></InputStyled>
        <button onClick={handleLog}>Connexion</button>
      </form>
    </div>
  )
}

const InputStyled = styled.input`
  margin: 20px 20px;
`
export default LoginForm
