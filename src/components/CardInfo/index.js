import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

const CardInfo = ({ ecusson, name, id }) => {
  return (
    <DivStyled primary>
      <ImgStyled src={ecusson}></ImgStyled>
      <h3>
        <Link to={`/teams/${id}`}>{name}</Link>
      </h3>
    </DivStyled>
  )
}

const ImgStyled = styled.img`
  height: 30px;
  width: 30px;
`
const DivStyled = styled.div`
  text-align: center;
  border: 1px solid black;
  margin: 10px 10px;
  padding: 10px 10px;
  width: 50%;
  margin-left: 25%;
  margin-right: 25%;
`

CardInfo.propTypes = {
  name: PropTypes.string,
  ecusson: PropTypes.string,
  id: PropTypes.number
}
export default CardInfo
