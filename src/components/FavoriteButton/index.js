import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import { Redirect, useHistory } from 'react-router-dom'

const FavoriteButton = ({ info }) => {
  const history = useHistory()
  const currentFavoris = localStorage.getItem('Favoris')
    ? JSON.parse(localStorage.getItem('Favoris'))
    : []
  const [exist, setExist] = useState(false)
  let Token = localStorage.getItem('Token')

  useEffect(() => {
    currentFavoris.map(item => {
      if (item.id === info.id) {
        setExist(true)
      }
    })
  }, [currentFavoris])

  const deleteFavorite = () => {
    const isPresent = currentFavoris.map(item => item.id).indexOf(info.id)

    if (Token) {
      if (isPresent >= 0) {
        const filtered = currentFavoris.filter(team => team.id !== info.id)
        localStorage.setItem('Favoris', JSON.stringify(filtered))
      }
    } else history.push('/noLog')
  }

  const addFavorite = () => {
    if (Token) {
      currentFavoris.push({ id: info.id, name: info.name })
      localStorage.setItem('Favoris', JSON.stringify(currentFavoris))
    } else {
      history.push('/noLog')
    }
  }

  return (
    <div>
      {exist ? (
        <ButtonStyled onClick={deleteFavorite}>Enlever Favoris</ButtonStyled>
      ) : (
        <ButtonStyled onClick={addFavorite}>Ajouter Favoris</ButtonStyled>
      )}
    </div>
  )
}

const ButtonStyled = styled.button`
  color: blue;

  &:hover {
    color: green;
  }
`
FavoriteButton.propTypes = {
  info: PropTypes.object
}

export default FavoriteButton
