import React from 'react'
import styled from 'styled-components'

import './menu.css'

import { Link } from 'react-router-dom'

const Menu = () => {
  const Token = localStorage.getItem('Token')
  return (
    <NavStyled>
      <ul>
        <li>
          <Link to='/'>Home</Link>
        </li>
        <li>
          <Link to='/favoris'>Favoris</Link>
        </li>
        {Token ? (
          <li>
            <Link to='/logout'>Logout</Link>
          </li>
        ) : (
          <li>
            <Link to='/login'>Login</Link>
          </li>
        )}
      </ul>
    </NavStyled>
  )
}

const NavStyled = styled.nav`
  background-color: whitesmoke;
  padding-top: 0.25%;
`

export default Menu
