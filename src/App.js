import React from 'react'
import './App.css'
import RouterMenu from './components/Router'

function App() {
  return (
    <div className='App'>
      <RouterMenu></RouterMenu>
    </div>
  )
}

export default App
