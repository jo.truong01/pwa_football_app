import React from 'react'
import styled from 'styled-components'
import { useHistory } from 'react-router-dom'

const Logout = () => {
  let history = useHistory()

  const handleLogout = () => {
    localStorage.removeItem('Token')
    history.push('/')
  }
  const handleNo = () => {
    history.push('/')
  }

  return (
    <DivError>
      <h2>êtes vous sur de nous quitter ?</h2>
      <span>
        <button onClick={handleLogout}>Oui</button>
        <button onClick={handleNo}>Non</button>
      </span>
    </DivError>
  )
}

const DivError = styled.div`
  margin-top: 10%;
  font-size: 30px;
`

export default Logout
