import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import Axios from 'axios'
import styled from 'styled-components'

import FavoriteButton from '../components/FavoriteButton'

const Teams = () => {
  const [ListPlayer, setListPlayer] = useState([])
  const [infoTeam, setInfo] = useState([])

  const id_team = useParams('id')
  const key_api = '1fd20851657843eab750d4d7bae0c06c'

  useEffect(() => {
    Axios.get(`http://api.football-data.org/v2/teams/${id_team.id}`, {
      headers: {
        'X-Auth-Token': key_api
      }
    })
      .then(response => {
        setInfo(response.data)
        setListPlayer(response.data.squad)
      })
      .catch(error => {
        console.error('ERROR: ', error)
      })
  }, [])

  return (
    <div>
      <span>
        <FavoriteButton info={infoTeam}></FavoriteButton>
      </span>
      <h1>{infoTeam.name}</h1>
      <img src={infoTeam.crestUrl}></img>
      <div>
        <h2>Composition equipe:</h2>
        <ul>
          {ListPlayer.length > 0 ? (
            ListPlayer.map(player => {
              return (
                <li key={player.id}>
                  {player.name} <em>Position:</em> {player.position}
                </li>
              )
            })
          ) : (
            <h2>Pas de joueur à {infoTeam.name}</h2>
          )}
        </ul>
      </div>
    </div>
  )
}

const ImgStyled = styled.img`
  height: 75px;
  width: 75px;
`

export default Teams
