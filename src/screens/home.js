import Axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

import Pagination from '../components/Pagination'

const Home = () => {
  const [compet, setListCompet] = useState([])
  const [total_compet, setTotal] = useState(0)
  const [pageCount, setPageCount] = useState(1)

  useEffect(() => {
    const key_api = '1fd20851657843eab750d4d7bae0c06c'
    Axios.get(`http://api.football-data.org/v2/competitions`, {
      headers: { 'X-Auth-Token': key_api }
    })
      .then(response => {
        setTotal(response.data.count)
        setListCompet(response.data.competitions)
      })
      .catch(error => console.error('ERROR : ', error))
  }, [pageCount])
  // ID ACCESSIBLE
  //: 2013 (serie A Bresil) / 2001 (Champions League) /
  //  2017 (liga Portugal) / 2021 (Premier League) / 2003 (Eredivisie)
  //  2016 (Championship Anglais) / 2014 (liga Espagne) / 2002 (Bundesliga)
  //  2015 (Ligue 1) / 2019 (Serie A italie) / 2018 (Euro 2020 Classement)
  return (
    <div>
      <h1>Select Competition Available:</h1>
      {compet.length > 0 ? (
        compet.map(item => {
          if (
            item.id === 2001 ||
            item.id === 2021 ||
            item.id === 2019 ||
            item.id === 2002 ||
            item.id === 2015 ||
            item.id === 2014 ||
            item.id === 2018
          )
            return (
              <div key={item.id}>
                <span>
                  <Link to={`/competition/${item.id}`}>{item.name}</Link>
                </span>
              </div>
            )
        })
      ) : (
        <h2>Pas de liste de competition Accessible</h2>
      )}
      <h1>Select your Competition (All Compet): </h1>
      {compet.length > 0 ? (
        compet.slice(pageCount * 10 - 10, pageCount * 10).map(item => (
          <div key={item.id}>
            <Link to={`/competition/${item.id}`}>{item.name}</Link>
          </div>
        ))
      ) : (
        <div>Pas de compétions disponible </div>
      )}
      <Pagination total={total_compet} setPageCount={setPageCount}></Pagination>
    </div>
  )
}

export default Home
