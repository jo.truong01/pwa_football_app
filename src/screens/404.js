import React from 'react'
import { useLocation } from 'react-router-dom'

const NoPage = () => {
  let location = useLocation()
  return <div>ERROR page not founed at : {location.pathname}</div>
}

export default NoPage
