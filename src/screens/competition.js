import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import Axios from 'axios'

import CardInfo from '../components/CardInfo'

const Competition = () => {
  let id_comp = useParams('id')

  const [competTeams, setCompetTeams] = useState([])
  const [nameCompet, setNameCompet] = useState('')

  useEffect(() => {
    const key_api = '1fd20851657843eab750d4d7bae0c06c'
    // ID ACCESSIBLE
    //: 2013 (serie A Bresil) / 2001 (Champions League) /
    //  2017 (liga Portugal) / 2021 (Premier League) / 2003 (Eredivisie)
    //  2016 (Championship Anglais) / 2014 (liga Espagne) / 2002 (Bundesliga)
    //  2015 (Ligue 1) / 2019 (Serie A italie) / 2018 (Euro 2020 Classement)
    Axios.get(
      `http://api.football-data.org/v2/competitions/${id_comp.id}/teams`,
      {
        headers: { 'X-Auth-Token': key_api }
      }
    )
      .then(response => {
        setNameCompet(response.data.competition.name)
        setCompetTeams(response.data.teams)
      })
      .catch(error => {
        console.error('ERROR COMPET: ', error)
      })
  }, [])
  return (
    <div>
      <h1>{nameCompet}</h1>
      {competTeams.length > 0 ? (
        competTeams.map(team => {
          return (
            <div key={team.id}>
              <CardInfo
                ecusson={team.crestUrl}
                name={team.name}
                id={team.id}
              ></CardInfo>
            </div>
          )
        })
      ) : (
        <h2>Pas d&apos;equipe dans cette competition</h2>
      )}
    </div>
  )
}

export default Competition
