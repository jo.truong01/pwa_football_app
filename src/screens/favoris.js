import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

const Favoris = () => {
  const currentFavoris = JSON.parse(localStorage.getItem('Favoris'))

  const [favoris, setFavoris] = useState(currentFavoris)

  return (
    <div>
      <h2>Vos Favoris:</h2>
      <ul>
        {favoris.length > 0 ? (
          favoris.map(team => {
            return (
              <li key={team.id}>
                <Link to={`/teams/${team.id}`}>{team.name}</Link>
              </li>
            )
          })
        ) : (
          <h3>Pas de favoris</h3>
        )}
      </ul>
    </div>
  )
}

export default Favoris
