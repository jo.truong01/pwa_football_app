import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

const NoLog = () => {
  return (
    <DivError>
      Vous devez vous connecter pour effecter cette action
      <Link to='/login'>Cliquez ici</Link>
    </DivError>
  )
}

const DivError = styled.div`
  margin-top: 10%;
  font-size: 30px;
`

export default NoLog
